<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(){
        $data=Role::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Role'
        ];

        return view('backend.master.role.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Role::find($id);
        }else{
            $data = new Role();
        }
        $params = [
            'title' => 'Manajemen Role',
            'data' => $data
        ];
        return view('backend.master.role.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Role::find($id);
        }else{
            $data = new Role();
        }
        $data->role_name = $request->role_name;
               
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Rule berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Rule gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Role::find($id)->delete();
            return "
            <div class='alert alert-success'>Rule berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Rule gagal dihapus!</div>";
        }

    }
}