<?php


namespace App\Http\Controllers\Api\Pegawai;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\Client;
use Illuminate\Http\Request;

class ApiListClientController extends Controller
{

    public function list($pegawai_code){

        $data = Client::where('pegawai_code',$pegawai_code)->orderBy('nama', 'ASC')->get();

        $detailClient=[];
        foreach ($data as $key => $item) {
                $detailClient[]=[
                'id_client'=>$item->id,
                'nama'=>$item->nama,
                'pekerjaan'=>$item->pekerjaan,
                'alamat'=>$item->alamat,
                'tgl_lahir'=>$item->tgl_lahir,
                'email'=>$item->email,
                'telp'=>$item->telp
            ];

        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get client Success!',
            'data' => $detailClient
        ];

        return response()->json($params);
    }


    public function search(Request $request,$pegawai_id)
    {
        $query = $request->input('query');
        if(is_null($query)){
            return response()
            ->json([
                'code'=>404,
                'description'=>'Not Found',
                'message'=>'parameter query not found',
                'data'=>[]
            ]);
        }

        if(is_null($pegawai_id)){
            return response()
            ->json([
                'code'=>404,
                'description'=>'Not Found',
                'message'=>'parameter pegawai not found',
                'data'=>[]
            ]);
        }

        $data = Client::whereRaw("nama like  '%".$query."%' ")
        ->where('pegawai_code',$pegawai_id)
            ->get();

        if($data->count()>0){
            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Get climbing tool Success!',
                'data' => $data
            ];
        }else{
            $params = [
                'code' => 404,
                'description' => 'Data not Found',
                'message' => 'Get climbing tool Success!',
                'data' => []
            ];
        }

       


        return response()->json($params);
    }


}