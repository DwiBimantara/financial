<?php

namespace App\Http\Controllers\Api\Pegawai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pegawai;

class ApiLoginPegawaiController extends Controller
{   

    public function login(Request $request){
        $apiName='VALIDATE_LOGIN';
        $code_agent=$request->code_agent;
        $password=sha1($request->password);

        if(is_null($code_agent)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter code agent!',
                'data'=>null
            ]);
        }

        $activeUser=Pegawai::where(['code_agent'=>$code_agent])->first();
        if(is_null($activeUser)){
            return response()->json([
                'code'=>404,
                'message'=>'Code Agent not Found!',
                'data'=>null
            ]);
        }

        if($activeUser->password != $password){
            return response()->json([
                'code'=>401,
                'message'=>'Password not Match!',
                'data'=>null
            ]);
        }

        $data = [
            'id' => $activeUser->id,
            'code_agent' => $activeUser->code_agent,
            'nama'=>$activeUser->nama,
            'email'=>$activeUser->email,
            'code_agent'=>$activeUser->code_agent,
            'tgl_lahir'=>$activeUser->tgl_lahir
        ];

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Login Success!',
            'data' => $data
        ];


        return response()->json($params);
    }

}