<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Pegawai;
use App\Models\Kategori;
use App\Models\Value;
use App\Models\Rule;
use App\Models\History;
use App\Models\HistoryStatement;
use DB;

class ApiHistoryController extends Controller
{ 
    public function list($client_id, Request $request){
         $start_date=null;
         $end_date=null;
        // $client_id=43;
         $start_date=$request->start_date;
         $end_date=$request->end_date;
         $end_date=date('Y-m-d', strtotime('+1 days', strtotime($end_date)));

            if ($start_date) {
                $data=History::select('created_at','client_id')->groupBy('created_at','client_id')->where('client_id',$client_id)->whereBetween('created_at', [$start_date, $end_date])->get();
                $data = DB::table('history as w')
                ->select(array(DB::Raw('DATE(w.created_at) day')))
                ->where('client_id',$client_id)
                ->whereBetween('created_at', [$start_date, $end_date])
                ->groupBy('day')
                ->orderBy('w.created_at')
                ->get();

                $params=[
                    'status'=>302,
                    'message'=>'success',
                    'data'=>$data
                ];
            } else {
                $data = DB::table('history as w')
                ->select(array(DB::Raw('DATE(w.created_at) day')))
                ->where('client_id',$client_id)
                //->whereBetween('created_at', [$start_date, $end_date])
                ->groupBy('day')
                ->orderBy('w.created_at')
                ->get();

                $params=[
                    'status'=>302,
                    'message'=>'success',
                    'data'=>$data
                ];
            }
            
            
        return response()->json($params);
    }

    public function detail($client_id, Request $request ){
        $date=null;
        $date=$request->date;
        //$client_id=$request->client_id;

        $dataRule=History::where('client_id',$client_id)->whereDate('created_at', '=', $date)->get();
        $curData=[];
        foreach ($dataRule as $key => $value) {
            $curData[]=[
                'value'=>number_format($value->value,2),
                'ratio'=>$value->getRatio->nama_ratio,
                'rule_statement'=>$value->getHistoryStatement->getRule->statement,
                'rule_saran'=>$value->getHistoryStatement->getRule->saran,
            ];
        }
        return response()->json([
                'code'=>302,
                'message'=>'Success!',
                'dataRule'=>$curData
            ]);
    }
    
}