<?php

namespace App\Http\Middleware;


class VerifyLogin
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        if (empty(session('activeUser'))) {
            return redirect('/login');
        } else {
            return $next($request);
        }
    }
}