<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Ratio extends Model
{
    protected $table = 'ratio';
    protected $primaryKey = 'id';
    public $timestamps = true; 
}