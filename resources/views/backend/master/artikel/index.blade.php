@extends('layout.main')
@section('title', $title)
@section('content')
<div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>{{$title}}</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Administrator</a> </li>
                        <li class="breadcrumb-item"><a href="#">Manajemen Pengguna</a> </li>
                        <li class="breadcrumb-item"><a href="{{url('/backend/master/artikel/')}}">{{$title}}</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
 
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <a onclick="loadModal(this)" target="/backend/master/artikel/form" class="btn btn-primary" title="Tambah Data"><i
                                    class="glyphicon glyphicon-plus"></i> Tambah Data</a>
                        <div class="table-responsive">
                            <div class="ibox-content">
 
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="table-role" >
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Title</th>
                                            <th>Text Header</th>
                                            <th>Text</th>
                                            <th>Image</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $num => $item)
                                            <tr>
                                                <td>{{$num+1}}</td>
                                                <td>{{$item->tittle}}</td>
                                                <td>{{$item->text_header}}</td>
                                                <td>{{$item->text}}</td>
                                                <td>{{$item->images}}</td>
                                                <td>
                                                    <img src="{{asset('public/upload/Artikel')}}/{{$item->images}}" class="img-responsive" alt="logo" width="80">
                                                </td>
                                                <td>
                                                    <a onclick="loadModal(this)" target="/backend/master/artikel/form" data="id={{$item->id}}"
                                                       class="btn btn-primary btn-xs glyphicon glyphicon-pencil" title="Ubah Data"></a>
                                                    <a onclick="deleteData({{$item->id}})" class="btn btn-danger btn-xs glyphicon glyphicon-trash"
                                                       title="Hapus Data"></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);
 
            modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
                ajaxTransfer("/backend/master/artikel/delete", data, "#modal-output");
            })
        }
 
        var table = $('#table-role');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
 
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
 
@endsection