<div id="result-form-konten"></div>
<div class="card-block">
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='ratio_id' class='col-sm-2 col-xs-12 control-label'>Pada Ratio</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-list"></i></span>
            <select name="ratio_id" class="form-control">
                @foreach($ratio as $num => $item)
                <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->ratio_id) selected="selected" @endif @endif >{{$item->nama_ratio}}
                </option>
                @endforeach
            </select>
            </div>
        </div>
    </div> 
    
    <div class='form-group'>
        <label for='range' class='col-sm-2 col-xs-12 control-label'>Range</label> 
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-bag-alt"></i></span>
            <input type="text" name="range" class="form-control" value="{{$data->range}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'>
        <label for='nilai' class='col-sm-2 col-xs-12 control-label'>Nilai</label> 
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-bag-alt"></i></span>
            <input type="text" name="nilai" class="form-control" value="{{$data->nilai}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'>
        <label for='statement' class='col-sm-2 col-xs-12 control-label'>Statement</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
            <input type="text" name="statement" class="form-control" value="{{$data->statement}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'>
        <label for='saran' class='col-sm-2 col-xs-12 control-label'>Saran</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-ui-email"></i></span>
            <input type="text" name="saran" class="form-control" value="{{$data->email}}" required="">
            </div>
        </div>
    </div> 
    
    

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/rule/save', data, '#result-form-konten');
        })
    })
</script>
