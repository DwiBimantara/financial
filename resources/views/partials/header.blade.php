<head>
    <title>MRT Stars</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('public/assets/template/')}}/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/template/')}}/icon/feather/css/feather.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/template/')}}/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/template/')}}/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/core/')}}/ajax.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/icofont/css/icofont.css">
     <link rel="stylesheet" type="text/css" href="{{asset('public/assets//bower_components')}}/datedropper/css/datedropper.min.css">
     <link rel="icon" href="../files/assets/images/favicon.ico" type="image/x-icon">
     <link rel="stylesheet" type="text/css" href="{{asset('public/assets/template/')}}/icon/themify-icons/themify-icons.css">
     
     <link rel="stylesheet" type="text/css" href="{{asset('public/assets/template/')}}/pages/advance-elements/css/bootstrap-datetimepicker.css">
      <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" >
      <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/spectrum/css/spectrum.css" >
      <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/jquery-minicolors/css/jquery.minicolors.css">
      <!-- Data Table Css -->
      <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" type="text/css" href=".{{asset('public/assets/template')}}/pages/data-table/css/buttons.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="{{asset('public/assets/')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
        


@yield('style')
</head>